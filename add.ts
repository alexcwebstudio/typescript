// 1. Write a function that returns the sum of two numbers.
// 2. Write a function that returns the sum of all numbers regardless of # of params.
//---\\

function add(numOne: number, numTwo:number) {
    return numOne + numTwo;
  }
  console.log(add(1,4));
  
function add2(...param1:number []):number {
    let total = 0;
    param1.forEach((num) => {
      total += num;
    })
    return total
  }

  console.log(add(3,4))
  console.log(add2(1,3,4,56,7));