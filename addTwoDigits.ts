//You are given a two-digit integer n. Return the sum of its digits.
//--\\
function addTwoDigits(n: number): number {
    let split = String(n).split("", 2);
    let answer = parseInt(split[0]) + parseInt(split[1]);
    return answer;
}

console.log(addTwoDigits(29));