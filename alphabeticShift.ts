//Given a string, replace each its character by the next one in the English alphabet (z would be replaced by a).
//---\\
function alphabeticShift(inputString: any): any {
    let newArray = [];
    for(let i = 0; i < inputString.length; i++) {
        if ( inputString.charCodeAt([i]) == 122 ) {
            newArray.push(String.fromCharCode(97));
        }
        else {
            newArray.push(String.fromCharCode(inputString.charCodeAt([i]) + 1));
        }
    }
    return newArray.join('')
}

console.log(alphabeticShift('crazy'));