//Given an array of strings, return another array containing all of its longest strings.
//--\\
function allLongestStrings(inputArray: string[]): any {
    let newArray = [];
    let largestValue = inputArray.map(w => w.length).pop();
    inputArray.forEach(function(letters){
        if(letters.length == largestValue) {
           newArray.push(letters)
        } 
    })
    return newArray
}

console.log(allLongestStrings(["aba", "aa", "ad", "vcd", "aba"])); 